# Example Project

To test this project please clone this repo, then run `npm install` followed by `bower install` this will pull down various modules that have been written. Once installed please checkout **src/packages/libs** for the various reusable custom built modules to see how they hook together.

An example of how we could be building modules can be seen in **src/packages/application.js** and **src/packages/toggle/toggle.js**

Javascript modules work purely on hooks, this means that we don't have to worry about calling functions, we call hooks, these hooks can be set to be called when content is ajaxed in so for example accordions can be reinitialized when contnet changes.

Templating system to follow, this is currently WIP and is installed as a node module from our internal git repo
