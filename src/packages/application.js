
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Core Modules
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
import {dom} from './libs/dom/dom';
import {events} from './libs/events/events';

// import the event manager to call methods
import EventManager from './libs/events/EventManager';
import EventDispatcher from './libs/events/EventDispatcher';

// turn on debugging, turn this off for production, or tie it in with a query string
EventManager.debug				= false;

//
EventManager.add_action('page_ready', [EventDispatcher, 'init']);

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Module: Toggle
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
import toggle from './libs/toggle/toggle';

// bind the init_content event to the initialization of the toggle module
EventManager.add_action('init_content', [toggle, 'init']);

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Module: Alert
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
import popup from './libs/alert/alert';

// bind the init_content event to the initialization of the toggle module
EventManager.add_action('init_content', [popup, 'init']);

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Module: Updater
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
import updater from './libs/updater/updater';

// bind the init_content event to the initialization of the toggle module
EventManager.add_action('init_content', [updater, 'init']);

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Module: Overlay
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
import overlay from './libs/overlay/overlay';

// bind the init_content event to the initialization of the toggle module
EventManager.add_action('page_ready', [overlay, 'init']);

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Module: Form
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
import validator from './libs/form/validator';

import './libs/form/validators/mmyy';
import './libs/form/validators/postcode';
import './libs/form/validators/sortcode';
import './libs/form/validators/creditcard';

import './libs/form/addons/mask';
import './libs/form/addons/fields';
import './libs/form/addons/email';
import './libs/form/addons/password';
import './libs/form/addons/multistep';

import ajax from './libs/form/ajax';

import './libs/form/credit-card';

// bind the init_content event to the initialization
EventManager.add_action('page_ready', [validator, 'init']);
EventManager.add_action('page_ready', [ajax, 'init']);

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Module: Map
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
import map from './libs/map/map';

// bind the init_content event to the initialization of the toggle module
EventManager.add_action('init_content', [map, 'init']);

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ...
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// when the page is ready, call all the events to start the javascript
events.ready(function()
{
	dom.addClass(document.querySelector('html'), 'js');
	dom.removeClass(document.querySelector('html'), 'no-js');

	EventManager.do_action('page_ready');

	// initialize all the content modules and set everything up
	EventManager.do_action('init_content');

	// if there is a hash, trigger the hash change on the page onload
	if(window.location.hash)
	{
		// there may be noting that is called from this hook.. however, in this case there is, we
		// watch for this hook being triggered in toggle.js
		EventManager.do_action('hash_change', window.location.hash);
	}
});

// add a hashchange watch, for changes in the hash and trigger the hash change action
events.on(window, 'hashchange', function(ev)
{
	EventManager.do_action('hash_change', window.location.hash);
});

import './libs/eq/eq';

import './libs/counter/counter';

import './development';
