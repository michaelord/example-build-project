
import {dom} from './libs/dom/dom';
import {events} from './libs/events/events';
import EventManager from './libs/events/EventManager';

import {verge} from './libs/dom/verge';
import {ElementData} from './libs/dom/ElementData';

import * as strings from './string';
import {escape} from './string';

/*

EventManager.add_action('form/validated', function(form, valid)
{
	let buttons					= form.querySelectorAll('button');

	[].forEach.call(buttons, function(button, i)
	{
		if(valid)
		{
			button.removeAttribute('disabled');
		}
		else
		{
			button.setAttribute('disabled', true);
		}
	});

}, 10, 2);

EventManager.add_action('form/field/valid', function(field)
{
	EventManager.do_action('validateform', dom.getClosest(field, 'form'), true, false);

}, 10, 1);

*/

EventManager.add_action('page_ready', function()
{
	events.on(window, 'scroll', function()
	{
		let y					= verge.scrollY();
		dom[(y > 200 ? 'add' : 'remove') + 'Class'](document.querySelector('html'), 'js-scrolled--screen');
	});

	var sections				= document.querySelectorAll('.section');
	var last_class				= null;

	events.on(window, 'scroll', function()
	{
		var in_view				= false;

		[].forEach.call(sections, function(section, index, object)
		{
			var rectangle		= verge.rectangle(section);
			var el_class		= section.getAttribute('data-class');

			if(rectangle.top <= 0 && rectangle.bottom > 0)
			{
				last_class		= el_class;
				in_view			= true;

				dom.addClass(document.querySelector('.section-test'), el_class);
			}
			else if(last_class != el_class)
			{
				dom.removeClass(document.querySelector('.section-test'), el_class);
			}

		});

		if(!in_view && last_class)
		{
			dom.removeClass(document.querySelector('.section-test'), last_class);

			last_class			= null;
		}
	});

	let params					=
	{
		data_name				: 'inview'
	};
	let elements				= document.querySelectorAll('[data-' + params.data_name + ']');
	let element_list			= [];

	[].forEach.call(elements, function(element, i)
	{
		element_list.push(ElementData.get(params.data_name, element, {once : true, inview : null, el : element}));

		element.removeAttribute('data-' + params.data_name)
	});

	EventManager.add_action('element/inview', function(element, is_in)
	{
		if(is_in)
		{
			dom.addClass(element, 'inview');
		}
		else
		{
			dom.removeClass(element, 'inview');
		}
	}, 10, 2);

	events.on(window, 'scroll', function()
	{
		[].forEach.call(element_list, function(data, index, object)
		{
			let in_view			= verge.inViewport(data.el);

			if(in_view && !data.inview)
			{
				EventManager.do_action('element/inview' + (data.trigger ? "?trigger=" + data.trigger : ""), data.el, true);

				if(data.once)
				{
					object.splice(index, 1);
				}
			}
			else if(!in_view && data.inview)
			{
				EventManager.do_action('element/inview' + (data.trigger ? "?trigger=" + data.trigger : ""), data.el, false);
			}

			data.inview			= in_view;
		});
	});

	// https://github.com/ryanve/verge
	// https://gist.github.com/jjmu15/8646226

	function trim(str)
	{
		return str.trim ? str.trim() : str.replace(/^\s+|\s+$/g, '');
	}

	function isArray(obj)
	{
		return (/Array/).test(Object.prototype.toString.call(obj));
	}

	function isDate(obj)
	{
		return (/Date/).test(Object.prototype.toString.call(obj)) && !isNaN(obj.getTime());
	}

	function is_touch_device()
	{
		return (('ontouchstart' in window) || (navigator.MaxTouchPoints > 0) || (navigator.msMaxTouchPoints > 0));
	}

	function is_mobile()
	{
		return false;
	}

	function is_tablet()
	{
		return false;
	}

	function is_input_supported(type = 'date')
	{
		let not_valid			= 'not-valid',
			input				= document.createElement('input');

		input.setAttribute('type', type);
		input.setAttribute('value', not_valid);

		return (input.value !== not_valid);
	}

	if(!is_touch_device())
	{
		console.log("datepicker");
	}

/*

	// get a list of all the forms on the page
	let forms					= document.querySelectorAll('form[data-validate]');

	// loop through all the forms
	[].forEach.call(forms, function(form, i)
	{
		let is_valid			= EventManager.do_action('validateform', form, true, false);

		console.log(is_valid);
	});

*/

	EventManager.add_action('hider', function(element)
	{
		if(element)
		{
			dom.addClass(element, 'hide');
		}

		try
		{
			let elements			= element.querySelectorAll('input:not(disabled), textarea:not(disabled), select:not(disabled)');

			[].forEach.call(elements, function(input, i)
			{
				input.setAttribute('data-hider', "true");
				input.setAttribute('disabled', true);
			});
		}
		catch(e)
		{

		}
	});

	EventManager.add_action('shower', function(element)
	{
		if(element)
		{
			dom.removeClass(element, 'hide');
		}

		try
		{
			let elements			= element.querySelectorAll('input[data-hider], textarea[data-hider], select[data-hider]');

			[].forEach.call(elements, function(input, i)
			{
				input.removeAttribute('data-hider');
				input.removeAttribute('disabled');
			});
		}
		catch(e)
		{

		}
	});

	EventManager.do_action('hider', document.querySelector('#doof'));

	setTimeout(function()
	{
		EventManager.do_action('shower', document.querySelector('#doof'));
	}, 20000)

	var p					= new Promise(function(resolve, reject)
	{
		setTimeout(function()
		{
			// DO logic

			if(true)
			{
				resolve("success");
			}
			else
			{
				reject("fail");
			}

		}, 2000);
	});

	p.then(function()
	{
		console.log("YYY");
	}).catch(function()
	{
		console.log("NNN");
	});

	let dismissables			= document.querySelectorAll('[data-dismiss]');
	let close					= '<button class="ui ui--close" data-close><i aria-hidden="true" class="icon icon--cross icon--sm"><svg><use xlink:href="/assets/icons.svg#cross"></use></svg></i></button>';

	[].forEach.call(dismissables, function(element, i)
	{
		let container			= element.querySelector(element.getAttribute('data-dismiss'));

		if(container)
		{
			let button			= dom.create(close).firstChild;

			events.on(button, 'click', function(ev)
			{
				events.cancel(ev);

				alert("TODO: Close");
			});

			dom.prepend(container, button);
		}
	});

	let aaa						= document.querySelector('#pooper');
	let els						= document.querySelectorAll('.nav--mobile a');

	[].forEach.call(els, function(element, i)
	{
		events.on(element, 'click', function(ev)
		{
			let parent			= dom.getClosest(this, '.nav__level');

			if(parent)
			{
				let x			= parent.className.match(/nav__level-([0-9]+)/);

				if(x)
				{
					let d			= Number(x[1]);

					dom.removeClass(gap, 'active-' + d);

					//console.log(gap)

					if(dom.containsClass(this, 'back'))
					{
						d--;
					}
					else
					{
						d++;
					}

					//console.log(d);
					dom.addClass(gap, 'active-' + d);
				}
			}

			events.cancel(ev);
		});
	});
}, 100);
