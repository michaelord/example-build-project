/*!
 * Prettylittle Framework Gulpfile
 * Copyright 2016 TH_NK
 * Licensed under MIT (...url...)
 */

var config						= require(process.cwd() + "/src/config.json");

// =============================================================================
// Libraries
// =============================================================================

var gulp						= require('gulp'),
	requireDir					= require('require-dir'),
	args						= require('yargs').argv,
	sequence					= require('gulp-sequence'),
	utils						= require('build-engine/utils');

// =============================================================================
// Tasks
// =============================================================================
require('build-engine/tasks/icons');
require('build-engine/tasks/install');
require('build-engine/tasks/media');
require('build-engine/tasks/scripts');
require('build-engine/tasks/styles');

// load all of the tasks
//requireDir('node_modules/build-engine/tasks', { recursive : true } );

// =============================================================================
// Tasks
// =============================================================================

var build_engine				= require('build-engine');
var _build_engine;
var style_engine				= require('build-engine/styleguide');
var icon_engine					= require('build-engine/icon');

var browserSync					= require('browser-sync'),
	watch						= require('gulp-watch');

gulp.task('data', function()
{
	icon_engine
	(
		{
			dir					: '/src/icons/',
			ext					: '.svg',
			output				: 'data/icon.json',
			onupdate			: function()
			{
				_build_engine.data['icons']		= utils.require_uncached(utils.normalize(process.cwd() + "/data/icon.json"));
			}
		}
	);
});

gulp.task('templates', function()
{
	var username				= "[username]";

	if(process.env['USERNAME'])
	{
		username				= process.env['USERNAME'];
	}

	_build_engine				= new build_engine
	(
		{
			port				: config.port,
			modules				: true,//
			reports				: true,//
			styleguide			: true,
			brand				: true,
			templates			: true,
			cssdocs				: true,
			jsdocs				: true,//
			application			: 'data/application.json',
			settings			: 'src/settings.json',
			params				: {
				"site-name"		: "Prettylittle",
				"username"		: username,
				"version"		: "v1.1"
			}
		}
	);
});

gulp.task('styleguide', function()
{
	style_engine
	(
		{
			styles				: '/src/packages/',
			output				: 'data/application.json',
			persistent			: false,
			main				: "application.scss"
		}
	);
});

gulp.task('serve', function()
{
	browserSync.init(
	{
		proxy					: "http://localhost:" + config.port,
		port					: config.port + 1000,
		notify					: false,
		reloadDebounce			: 100,
		logPrefix				: "Template Engine"
	});

	files			   = [
		'./gulp/settings.json',
		'./assets/css/*.css',
		'./assets/js/*.js',
		'./assets/icons/*.svg',
		'./templates/views/*.html',
		'./templates/views/**/*.html',
		'./templates/shared/*.html',
		'./templates/shared/**/*.html',
		'./src/**/*.html'
	];

	watch(files, {}, function()
	{
		browserSync.reload();
	});
});

gulp.task('document', sequence(['scripts:doc']))

gulp.task('build', sequence(['styles', 'scripts', /*'document',*/ 'media', 'icons'], ['styles:watch', 'scripts:watch', 'icons:watch'], 'serve'))

gulp.task('default', sequence(['styles', 'scripts', 'icons'], ['styles:watch', 'scripts:watch'], 'templates', 'styleguide', 'data', 'serve'));

